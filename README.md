## The plugin is intended for cutting the image into parts and packaging these parts in a square atlas.

![picture](https://bitbucket.org/dreamdale/backgroundpacker/raw/8664c155ce58451218d78eaa6eedee8c05ca9649/Documentation~/Bgpacker.jpg)

Select an image(images). Right click and select menu item "Background Packer/Create packed atlas". The plugin will chop and create a square atlas with sprites.

Select packed texture. Right click and you can create a mesh or a set of sprites from a packed texture.

## Set up options

In "Unity/Preferences.../Background Packer" you can set up settings.

![settings](https://bitbucket.org/dreamdale/backgroundpacker/raw/d56cb4e5a9d75978cdb1e384730c12d2a4bec5b0/Documentation~/BgpackerSettings.jpg)
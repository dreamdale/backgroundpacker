﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Kea {
	public class BackgroundPacker {
		static readonly int[] allowedResolutions = { 512, 1024, 2048, 4096, 8192 };
		static readonly int[] allowedMargins = { 1, 2, 4, 8 };

		const int DEFAULT_ATLAS_RESOLUTION = 2048;
		const string ATLAS_RESOLUTION_KEY = "ATLAS_RESOLUTION_KEY";
		public static int atlasResolution = DEFAULT_ATLAS_RESOLUTION;

		const int DEFAULT_MARGIN = 2;
		const string MARGIN_KEY = "MARGIN_KEY";
		public static int margin = DEFAULT_MARGIN;

		const string DEFAULT_SHADER = "BackgroundPacker/BackgroundPackerShader";
		const string SHADER_KEY = "SHADER";
		public static string defaultShader = DEFAULT_SHADER;

		const string DEFAULT_ATLAS_TEMPLATE = "{0}_Packed";
		const string ATLAS_TEMPLATE_KEY = "ATLAS_TEMPLATE";
		public static string atlasNameTemplate = DEFAULT_ATLAS_TEMPLATE;

		const string DEFAULT_SPRITE_TEMPLATE = "Sprite_{0}_{1}";
		const string SPRITE_TEMPLATE_KEY = "SPRITE_TEMPLATE";
		public static string spriteNameTemplate = DEFAULT_SPRITE_TEMPLATE;

		const string DEFAULT_MESH_TEMPLATE = "{0}_Mesh";
		const string MESH_TEMPLATE_KEY = "MESH_TEMPLATE";
		public static string meshNameTemplate = DEFAULT_MESH_TEMPLATE;

		const string DEFAULT_MATERIAL_TEMPLATE = "{0}_Material";
		const string MATERIAL_TEMPLATE_KEY = "MATERIAL_TEMPLATE";
		public static string materialNameTemplate = DEFAULT_MATERIAL_TEMPLATE;

		const string CREATE_SPRITES_ON_TEMPLATE_KEY = "CREATE_SPRITES_ON_TEMPLATE";
		public static bool createSpritesOnTemplate = true;

		const string FILL_COLOR_KEY = "FILL_COLOR_KEY";
		public static Color fillColor = Color.magenta;

		const int marginCount = 2;

		static GUIContent[] gs = null;
		static GUIContent[] ms = null;

		static bool IsInited {
			get {
				return (gs != null && ms != null);
			}
		}

		static BackgroundPacker() {
			Init();
		}

		static void Init() {
			atlasResolution = EditorPrefs.GetInt(ATLAS_RESOLUTION_KEY, DEFAULT_ATLAS_RESOLUTION);
			margin = EditorPrefs.GetInt(MARGIN_KEY, DEFAULT_MARGIN);
			defaultShader = EditorPrefs.GetString(SHADER_KEY, DEFAULT_SHADER);
			atlasNameTemplate = EditorPrefs.GetString(ATLAS_TEMPLATE_KEY, DEFAULT_ATLAS_TEMPLATE);
			spriteNameTemplate = EditorPrefs.GetString(SPRITE_TEMPLATE_KEY, DEFAULT_SPRITE_TEMPLATE);
			meshNameTemplate = EditorPrefs.GetString(MESH_TEMPLATE_KEY, DEFAULT_MESH_TEMPLATE);
			materialNameTemplate = EditorPrefs.GetString(MATERIAL_TEMPLATE_KEY, DEFAULT_MATERIAL_TEMPLATE);
			createSpritesOnTemplate = EditorPrefs.GetBool(CREATE_SPRITES_ON_TEMPLATE_KEY, true);
			ColorUtility.TryParseHtmlString(EditorPrefs.GetString(FILL_COLOR_KEY, ColorUtility.ToHtmlStringRGBA(Color.clear)), out fillColor);
			gs = new GUIContent[allowedResolutions.Length];
			for (int i = 0; i < allowedResolutions.Length; i++) {
				gs[i] = new GUIContent(allowedResolutions[i].ToString(), "Current atlas resolution");
			}

			ms = new GUIContent[allowedMargins.Length];
			for (int i = 0; i < allowedMargins.Length; i++) {
				ms[i] = new GUIContent(allowedMargins[i].ToString(), "Current margin");
			}
		}

		static RectInt CanPlace(RectInt atlasTextureRect, RectInt spriteTextureRect) {
			RectInt result = spriteTextureRect;
			if (spriteTextureRect.width + margin * marginCount > atlasTextureRect.width) {
				result.x = spriteTextureRect.x;
				result.width = atlasTextureRect.width - margin * marginCount;
			}
			if (spriteTextureRect.height + margin * marginCount > atlasTextureRect.height) {
				result.y = spriteTextureRect.y;
				result.height = atlasTextureRect.height - margin * marginCount;
			}

			return result;
		}

		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create packed atlas", true)]
		static bool CreatePackedAtlasValidate() {
			foreach (var selection in Selection.objects) {
				var texture = selection as Texture2D;
				if (texture == null) {
					return false;
				}
			}
			return true;
		}

		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create packed atlas")]
		static void CreatePackedAtlas() {
			int i = 0;
			foreach (var selection in Selection.objects) {
				float progress = (float)(i++) / (float)Selection.objects.Count();
				Texture2D texture = selection as Texture2D;
				EditorUtility.DisplayProgressBar("Pack Textures", "Pack texture " + texture.name, progress);
				CreatePackedAtlas(texture);
			}
			EditorUtility.ClearProgressBar();
		}

		static void CreatePackedAtlas(Texture2D texture) {
			string texturePath = AssetDatabase.GetAssetPath(texture);

			RectInt atlasTextureRect = new RectInt(0, 0, atlasResolution, atlasResolution);
			RectInt spriteTextureRect = new RectInt(0, 0, (int)texture.width, (int)texture.height);
			var spriteArea = (spriteTextureRect.width + margin * marginCount) * (spriteTextureRect.height + margin * marginCount);
			var atlasArea = atlasTextureRect.width * atlasTextureRect.height;

			if (spriteArea > atlasArea) {
				Debug.LogError(string.Format("Can't fit texture {0} - it is too big", texture.name));
				return;
			}

			TextureImporter importer = (TextureImporter)AssetImporter.GetAtPath(texturePath);
			if (createSpritesOnTemplate) {
				if (importer.textureType != TextureImporterType.Sprite || importer.spriteImportMode != SpriteImportMode.Multiple) {
					importer.textureType = TextureImporterType.Sprite;
					importer.spriteImportMode = SpriteImportMode.Multiple;
					AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
				}
			}
			if (!importer.isReadable) {
				importer.isReadable = true;
				AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
			}

			List<SpriteMetaData> spriteList = new List<SpriteMetaData>();
			List<SpriteMetaData> atlasSpriteList = new List<SpriteMetaData>();

			float x_center = texture.width / 2f;
			float y_center = texture.height / 2f;
			bool loop = true;
			int i = 0;

			Texture2D atlasTexture = new Texture2D(atlasResolution, atlasResolution);
			Color[] colorArray = Enumerable.Repeat(fillColor, atlasResolution * atlasResolution).ToArray();
			atlasTexture.SetPixels(colorArray);

			while (loop) {
				atlasArea = atlasTextureRect.width * atlasTextureRect.height;
				var currentSpriteArea = (spriteTextureRect.width + margin * marginCount) * (spriteTextureRect.height + margin * marginCount);

				if (currentSpriteArea > atlasArea) {
					float persent = Mathf.Ceil((((float)currentSpriteArea / (float)atlasArea) - 1f) * 100f);
					Debug.LogError(string.Format("Can't fit texture {0} - need {1} % of atlas area more{2}",
						texture.name,
						persent,
						(persent < 5f && margin > 1) ? ", \nTry decrease margin" : ""));
					return;
				}

				RectInt result = CanPlace(atlasTextureRect, spriteTextureRect);

				// set up sprite pivot
				float pivot_x = (x_center - (float)result.x) / (float)result.width;
				float pivot_y = (y_center - (float)result.y) / (float)result.height;

				SpriteMetaData meta = new SpriteMetaData {
					name = string.Format(spriteNameTemplate, texture.name, i),
					rect = new Rect(result.x, result.y, result.width, result.height),
					alignment = 9, // custom pivot
					pivot = new Vector2(pivot_x, pivot_y)
				};

				spriteList.Add(meta);

				var spriteTexture = texture.TextureFromSpriteMetaData(meta);
				var spriteTextureWithMargin = spriteTexture.Margins(margin);
				var spriteTextureBytes = spriteTextureWithMargin.GetPixels();

				var t_x = atlasResolution - atlasTextureRect.width;
				var t_y = atlasResolution - atlasTextureRect.height;
				atlasTexture.SetPixels(t_x, t_y, spriteTextureWithMargin.width, spriteTextureWithMargin.height, spriteTextureBytes);

				var atlasSpriteRect = meta.rect;
				atlasSpriteRect.x = t_x + margin;
				atlasSpriteRect.y = t_y + margin;
				SpriteMetaData atlasSpriteMeta = new SpriteMetaData {
					name = meta.name,
					rect = atlasSpriteRect,
					alignment = meta.alignment,
					pivot = meta.pivot
				};
				atlasSpriteList.Add(atlasSpriteMeta);

				i++;

				if (Equals(result, spriteTextureRect)) {
					Debug.Log(string.Format("Texture '{0}' packed", texture.name));
					loop = false;
					continue;
				}

				if (result.height == spriteTextureRect.height) {
					spriteTextureRect.x += result.width;
					spriteTextureRect.width -= result.width;
					atlasTextureRect.height -= result.height + margin * marginCount;
				} else {
					spriteTextureRect.y += result.height;
					spriteTextureRect.height -= result.height;
					atlasTextureRect.width -= result.width + margin * marginCount;
				}
			}


			var folder = Path.GetDirectoryName(texturePath);
			var fileName = string.Format(atlasNameTemplate, texture.name);
			fileName += ".png";
			var atlasPath = Path.Combine(folder, fileName);
			File.WriteAllBytes(atlasPath, atlasTexture.EncodeToPNG());

			EditorUtility.SetDirty(atlasTexture);
			AssetDatabase.ImportAsset(atlasPath, ImportAssetOptions.ForceUpdate);

			TextureImporter atlasImporter = (TextureImporter)AssetImporter.GetAtPath(atlasPath);
			atlasImporter.textureType = TextureImporterType.Sprite;
			atlasImporter.spriteImportMode = SpriteImportMode.Multiple;
			atlasImporter.spritesheet = atlasSpriteList.ToArray();

			EditorUtility.SetDirty(atlasTexture);
			EditorUtility.SetDirty(atlasImporter);
			AssetDatabase.ImportAsset(atlasPath, ImportAssetOptions.ForceUpdate);

			if (createSpritesOnTemplate) {
				importer.spritesheet = spriteList.ToArray();
			}
			EditorUtility.SetDirty(texture);
			EditorUtility.SetDirty(importer);
			AssetDatabase.ImportAsset(texturePath, ImportAssetOptions.ForceUpdate);
		}


		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create Mesh GameObject", true)]
		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create Sprite GameObject", true)]
		static bool CreateMeshObjectValidate() {
			foreach (var selection in Selection.objects) {
				var texture = selection as Texture2D;
				if (texture == null) {
					return false;
				}
				if (!texture.IsSquare() || !texture.IsAllowedResolution(allowedResolutions)) {
					return false;
				}
			}
			return true;
		}

		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create Mesh GameObject")]
		static void CreateMeshObject() {
			foreach (var selection in Selection.objects) {
				Texture2D texture = selection as Texture2D;
				string texPath = AssetDatabase.GetAssetPath(texture);
				Sprite[] sprites = AssetDatabase.LoadAllAssetsAtPath(texPath).OfType<Sprite>().ToArray();

				CreateGameObject(selection.name, texture, sprites);
			}
		}

		static void CreateGameObject(string name, Texture2D texture, Sprite[] sprites) {
			var go = new GameObject(name);
			var meshFilter = go.AddComponent<MeshFilter>();
			var meshRenderer = go.AddComponent<MeshRenderer>();

			// disable unnecessary settings
			meshRenderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
			meshRenderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
			meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
			meshRenderer.receiveShadows = false;
			meshRenderer.allowOcclusionWhenDynamic = false;

			// generate mesh
			Mesh mesh = new Mesh();
			mesh.name = string.Format(meshNameTemplate, name);
			meshFilter.mesh = mesh;
			List<Vector3> allVerticles = new List<Vector3>();
			List<Vector2> allUvs = new List<Vector2>();
			List<int> allTriangles = new List<int>();
			int indexOffset = 0;
			foreach (var sprite in sprites) {
				allVerticles.AddRange(Array.ConvertAll(sprite.vertices, i => (Vector3)i).ToList());
				allUvs.AddRange(sprite.uv.ToList());
				allTriangles.AddRange(Array.ConvertAll(sprite.triangles, i => (int)(i + indexOffset)));
				indexOffset += sprite.vertices.Length;
			}
			mesh.SetVertices(allVerticles);
			mesh.SetUVs(0, allUvs);
			mesh.SetTriangles(allTriangles, 0);

			meshRenderer.material = GetMaterial(name);
			meshRenderer.sharedMaterial.SetTexture("_MainTex", texture);
			meshRenderer.sharedMaterial.SetColor("_Color", Color.white);
		}

		static Material GetMaterial(string name) {
			var materialName = string.Format(materialNameTemplate, name);
			var resources = Resources.FindObjectsOfTypeAll(typeof(Material));

			var material = resources.FirstOrDefault(mat => mat.name.Equals(materialName)) as Material;
			if (material == null) {
				material = new Material(Shader.Find(defaultShader));
				material.name = materialName;
			}
			return material;
		}


		[MenuItem("Assets/\ud83c\udf04 Background Packer/Create Sprite GameObject")]
		static void CreateSpriteObject() {
			foreach (var selection in Selection.objects) {
				Texture2D texture = selection as Texture2D;
				string texturePath = AssetDatabase.GetAssetPath(texture);
				Sprite[] sprites = AssetDatabase.LoadAllAssetsAtPath(texturePath).OfType<Sprite>().ToArray();
				CreateSpriteObject(texture.name, sprites);
			}
		}

		static void CreateSpriteObject(string name, Sprite[] sprites) {
			var go = new GameObject(name);
			foreach (var sprite in sprites) {
				var spriteGo = new GameObject(sprite.name);
				spriteGo.transform.SetParent(go.transform);
				var spriteRenderer = spriteGo.AddComponent<SpriteRenderer>();
				spriteRenderer.sprite = sprite;
			}
		}


		[PreferenceItem("Background Packer")]
		static void PreferencesGUI() {
			if (!IsInited)
				Init();

			EditorGUILayout.LabelField("Atlas creation settings", EditorStyles.boldLabel);

			EditorGUI.BeginChangeCheck();
			atlasResolution = EditorGUILayout.IntPopup(new GUIContent("Atlas resolution", "Select resolution for atlas"), atlasResolution, gs, allowedResolutions);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetInt(ATLAS_RESOLUTION_KEY, atlasResolution);
			}

			EditorGUI.BeginChangeCheck();
			margin = EditorGUILayout.IntPopup(new GUIContent("Margin", "Select margin,\n1 - if not use MipMaps,\n2-4-8 - if use MipMaps"), margin, ms, allowedMargins);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetInt(MARGIN_KEY, margin);
			}


			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Shader settings", EditorStyles.boldLabel);

			EditorGUI.BeginChangeCheck();
			var shader = (EditorGUILayout.ObjectField("Default Shader", Shader.Find(defaultShader), typeof(Shader), false) as Shader);
			defaultShader = shader != null ? shader.name : DEFAULT_SHADER;
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(SHADER_KEY, defaultShader);
			}


			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Name templates", EditorStyles.boldLabel);

			EditorGUI.BeginChangeCheck();
			atlasNameTemplate = EditorGUILayout.TextField(new GUIContent("Atlas name template", "{0} - original texture name"), atlasNameTemplate);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(ATLAS_TEMPLATE_KEY, atlasNameTemplate);
			}

			EditorGUI.BeginChangeCheck();
			spriteNameTemplate = EditorGUILayout.TextField(new GUIContent("Sprite name template", "{0} - original texture name\n{1} - current sprite number"), spriteNameTemplate);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(SPRITE_TEMPLATE_KEY, spriteNameTemplate);
			}

			EditorGUI.BeginChangeCheck();
			meshNameTemplate = EditorGUILayout.TextField(new GUIContent("Mesh name template", "{0} - original texture name"), meshNameTemplate);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(MESH_TEMPLATE_KEY, meshNameTemplate);
			}

			EditorGUI.BeginChangeCheck();
			materialNameTemplate = EditorGUILayout.TextField(new GUIContent("Material name template", "{0} - original texture name"), materialNameTemplate);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(MATERIAL_TEMPLATE_KEY, materialNameTemplate);
			}

			EditorGUI.BeginChangeCheck();
			createSpritesOnTemplate = EditorGUILayout.Toggle(new GUIContent("Create sprites on template", "If checked - sprites on template texture is created"), createSpritesOnTemplate);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetBool(CREATE_SPRITES_ON_TEMPLATE_KEY, createSpritesOnTemplate);
			}

			EditorGUI.BeginChangeCheck();
			fillColor = EditorGUILayout.ColorField(new GUIContent("Fill color of unused area", "Fill with this color all unused area"), fillColor);
			if (EditorGUI.EndChangeCheck()) {
				EditorPrefs.SetString(FILL_COLOR_KEY, ColorUtility.ToHtmlStringRGBA(fillColor));
			}
		}
	}
}
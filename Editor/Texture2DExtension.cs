﻿using System.Linq;
using UnityEngine;
using UnityEditor;

namespace Kea {
	public static class Texture2DExtension {
		public static Texture2D TextureFromSpriteMetaData(this Texture2D texture, SpriteMetaData sprite) {
			var spriteTexture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
			var pixels = texture.GetPixels((int)sprite.rect.x,
											(int)sprite.rect.y,
											(int)sprite.rect.width,
											(int)sprite.rect.height);
			spriteTexture.SetPixels(pixels);
			spriteTexture.Apply();
			return spriteTexture;
		}

		public static Texture2D Margins(this Texture2D texture, int margin) {
			Texture2D textureWithMargin = new Texture2D(texture.width + 2 * margin, texture.height + 2 * margin);

			var data = texture.GetPixels();
			textureWithMargin.SetPixels(margin, margin, texture.width, texture.height, data);

			for (int i = margin; i > 0; i--) {
				CopyHorizontalLine(textureWithMargin, i, i - 1);
				CopyHorizontalLine(textureWithMargin, textureWithMargin.height - i - 1, textureWithMargin.height - i);
				CopyVerticalLine(textureWithMargin, i, i - 1);
				CopyVerticalLine(textureWithMargin, textureWithMargin.width - i - 1, textureWithMargin.width - i);
			}

			textureWithMargin.Apply();
			return textureWithMargin;
		}

		private static void CopyHorizontalLine(this Texture2D texture, int sourceLineIndex, int destLineIndex) {
			var pixels = texture.GetPixels(0, sourceLineIndex, texture.width, 1);
			texture.SetPixels(0, destLineIndex, texture.width, 1, pixels);
		}

		private static void CopyVerticalLine(this Texture2D texture, int sourceColumnIndex, int destColumnIndex) {
			var pixels = texture.GetPixels(sourceColumnIndex, 0, 1, texture.height);
			texture.SetPixels(destColumnIndex, 0, 1, texture.height, pixels);
		}

		public static bool IsSquare(this Texture2D texture) {
			return (texture.width == texture.height);
		}

		public static bool IsAllowedResolution(this Texture2D texture, int[] allowedResolutions) {
			if (!allowedResolutions.Contains(texture.width)) { // is width pow of 2?
				return false;
			}

			if (!allowedResolutions.Contains(texture.height)) { // is height pow of 2?
				return false;
			}
			return true;
		}
	}
}